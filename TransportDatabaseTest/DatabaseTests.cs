﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using System.Web.Script.Serialization;
using System.IO;

namespace TransportDatabase.Tests
{
    [TestClass()]
    public class DatabaseTests
    {
        private Database _database;
        private string _nameDatabase;

        [TestInitialize]
        public void TestInitialize()
        {
            _database = new Database();
            _nameDatabase = "database.json";
        }

        [TestCleanup]
        public void TestCleanUp()
        {
            var fileTruck = new FileInfo(_nameDatabase);
            fileTruck.Delete();
        }

        [TestMethod()]
        public void AddItemCarNullTest()
        {
            _database.AddItem(new Car());
            var car = (Car)_database.GetDatabase()[0];

            if (1 == car.Id && 0 == car.Engine && 0 == car.MaxSpeed && 0 == car.Year)
            {
                Assert.IsTrue(true);
                return;
            }
            Assert.IsTrue(false);
        }

        [TestMethod()]
        public void AddItemCarDataTest()
        {
            _database.AddItem(new Car(144,"Car",78,21));
            var car = (Car)_database.GetDatabase()[0];

            if (1 == car.Id && 78 == car.Engine && 21 == car.MaxSpeed && 144 == car.Year && "Car" == car.Brand)
            {
                Assert.IsTrue(true);
                return;
            }
            Assert.IsTrue(false);
        }

        [TestMethod()]
        public void AddItemTruckNullTest()
        {
            _database.AddItem(new Truck());
            var truck = (Truck)_database.GetDatabase()[0];

            if (1 == truck.Id && 0 == truck.Engine && 0 == truck.LoadCarrying && 0 == truck.Year)
            {
                Assert.IsTrue(true);
                return;
            }
            Assert.IsTrue(false);
        }

        [TestMethod()]
        public void AddItemTruckDataTest()
        {
            _database.AddItem(new Truck(14,"Truck",145,600));
            var truck = (Truck)_database.GetDatabase()[0];

            if (1 == truck.Id && 145 == truck.Engine && 600 == truck.LoadCarrying && 14 == truck.Year && "Truck" == truck.Brand)
            {
                Assert.IsTrue(true);
                return;
            }
            Assert.IsTrue(false);
        }
    }
}