﻿using System;

namespace TransportDatabase
{
    public abstract class Item
    {
        public int Id { get; set; }

        public Item() { }

        public abstract string Show();
    }
}
