﻿namespace TransportDatabase
{
    public class Truck : Transport
    {
        public int LoadCarrying { get; set; }

        public Truck() { }

        public Truck(int year, string brand, int engine, int loadCarrying) : base(year, brand, engine)
        {
            LoadCarrying = loadCarrying;
        }

        public override string Show()
        {
            return $"ID: {Id} | Brand: {Brand} | Year: {Year} | Engine: {Engine} | load_carrying: {LoadCarrying}";
        }
    }
}
