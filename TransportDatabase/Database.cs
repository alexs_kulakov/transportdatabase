﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TransportDatabase
{
    public class Database
    {
        private List<Transport> _transport;
        private int _maxId;
        private string _nameDatabase;
        private JsonSerializerSettings _jsonSerializerSettings;

        public Database()
        {
            _jsonSerializerSettings = new JsonSerializerSettings()
            {
                TypeNameHandling = TypeNameHandling.All
            };
            _transport = new List<Transport>();
            _maxId = 0;
            _nameDatabase = "database.json";
            var json = UnloadDateFromFile(_nameDatabase);

            if (string.Empty != json)
            {
                _transport = JsonConvert.DeserializeObject<List<Transport>>(json, _jsonSerializerSettings);
                _maxId = _transport.Max(x => x.Id);

            }
        }

        public void AddItem(Transport item)
        {
            item.Id = ++_maxId;
            _transport.Add(item);
            saveToFile(_nameDatabase);
        }

        private void saveToFile(string fileName)
        {
            var json = JsonConvert.SerializeObject(_transport, Formatting.Indented, _jsonSerializerSettings);

            using (var streamWrite = new StreamWriter(fileName))
            {
                streamWrite.WriteLine(json);
            }
        }

        public List<Transport> GetDatabase()
        {
            return _transport;
        }

        public string UnloadDateFromFile(string fileName)
        {
            try
            {
                using (var streamReader = new StreamReader(fileName))
                {
                    return streamReader.ReadToEnd();
                }
            }
            catch (FileNotFoundException e)
            {
                return string.Empty;
            }
        }
    }
}
