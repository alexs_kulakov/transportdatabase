﻿namespace TransportDatabase
{
    public abstract class Transport : Item
    {
        public int Year { get; set; }
        public string Brand { get; set; }
        public int Engine { get; set; }

        public Transport() { }

        public Transport(int year, string brand, int engine) : base()
        {
            Year = year;
            Brand = brand;
            Engine = engine;
        }
    }
}
