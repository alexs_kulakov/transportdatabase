﻿ namespace TransportDatabase
{
    public class Car : Transport
    {
        public int MaxSpeed { get; set;}

        public Car() { }

        public Car(int year, string brand, int engine, int maxSpeed) : base(year, brand, engine)
        {
            MaxSpeed = maxSpeed;
        }

        public override string Show()
        {
            return $"ID: {Id} | Brand: {Brand} | Year: {Year} | Engine: {Engine} | Max_speed: {MaxSpeed}";
        }
    }
}
