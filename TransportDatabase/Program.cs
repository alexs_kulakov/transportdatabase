﻿using System;
using System.Collections.Generic;

namespace TransportDatabase
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Transport> transport;
            Database database = new Database();
            database.AddItem(new Car(1, "1", 1, 1));
            database.AddItem(new Truck(2, "2", 2, 2));
            database.AddItem(new Car(3, "3", 3, 3));
            database.AddItem(new Car(3, "3", 3, 3));
            database.AddItem(new Car(3, "3", 3, 3));
            transport = database.GetDatabase();
            foreach (Transport item in transport)
            {
                Console.WriteLine(item.Show());
            }
            Console.ReadLine();
        }
    }
}